import argparse
from pathlib import Path

import texts


def existing_path(parser, arg):
    path = Path(arg)
    if not path.exists():
        parser.error("The file %s does not exist!" % arg)
    else:
        return path


def embed_into_base(
    path: Path, title: str | None = None, description: str | None = None
):
    text = Path("base.html").read_text()
    content = path.read_text()

    text = text.replace("<<content>>", content)
    text = text.replace(
        "<<title>>",
        title or "Mikuláš Poul &middot; {}".format(texts.TEXTS["general"]["role"]),
    )
    text = text.replace(
        "<<description>>", description or texts.TEXTS["general"]["web_desc"]
    )

    path.write_text(text)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Embed page into the base.html")
    parser.add_argument(
        dest="input",
        help="File",
        metavar="INPUT",
        type=lambda x: existing_path(parser, x),
    )
    args = parser.parse_args()

    embed_into_base(args.input)
