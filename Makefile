SHELL := /bin/bash

all: index.html cv/english.pdf blogs

cv/english.tex: cv/english_template.tex format_template.py texts.py
	python3 format_template.py cv/english_template.tex cv/english.tex

cv/page1sidebar.tex: cv/page1sidebar_template.tex format_template.py texts.py
	python3 format_template.py cv/page1sidebar_template.tex cv/page1sidebar.tex

cv/english.pdf: cv/english.tex cv/page1sidebar.tex
	@if [ "${MAKE_LOCALLY}" == "true" ]; then \
		make -C cv english.pdf;\
	else\
		docker run -v `pwd`/cv:/srv/cv:Z -w /srv/cv -it mikicz/texlive-docker:40 make english.pdf;\
	fi\

base.html: templates/base.html format_template.py texts.py
	python3 format_template.py templates/base.html base.html

index.html: templates/index.html format_template.py texts.py base.html
	python3 format_template.py templates/index.html index.html
	python3 embed_into_base.py index.html

.PHONY: clean
clean:
	rm -rf public/
	git clean -fx *.html
	make -C cv clean

open: cv/english.pdf
	gio open cv/english.pdf 2>/dev/null

open_all: cv/english.pdf index.html
	gio open cv/english.pdf 2>/dev/null

.PHONY: blogs
blogs: templates/blog.html format_template.py texts.py base.html
	python3 make_all_blogs.py

.PHONY: public
public:
	rm -rf public/
	mkdir public/
	cp -R index.html static/ public/
	cp cv/english.pdf public/cv.pdf

	mkdir public/youtube/
	cp youtube_redirect.html public/youtube/index.html

	./make_blogs_public.sh

	mkdir public/talks/
	mv pytest-classes-talk/talks/pytest-classes-talk/ public/talks/
	mv django-aggregation-talk/talks/django-aggregation-talk/ public/talks/
	mkdir public/talks/ucl-photonics-oss-talks/
	mv ucl-photonics-oss-talks/talks/ucl-photonics-oss-talks/* public/talks/ucl-photonics-oss-talks/
