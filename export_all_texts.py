import re
from typing import Dict

from texts import TEXTS
from format_template import link_regex, bold_regex


def strip_text(text):
    text = text.replace("\\\\", " ")
    text = text.strip()

    matches = re.finditer(link_regex, text)

    for match in matches:
        original = match.group()
        groups = match.groups()

        text = text.replace(original, groups[1])

    matches = re.finditer(bold_regex, text)

    for match in matches:
        original = match.group()
        groups = match.groups()

        text = text.replace(original, groups[0])

    return text


def go_through_dict(data: Dict):
    for k, v in data.items():
        if isinstance(v, dict):
            go_through_dict(v)
        if k == "personal":
            for v2 in v.values():
                print(" ".join([strip_text(x) for x in v2.strip().splitlines()]))
                print("")
        if "desc" in k:
            print(" ".join([strip_text(x) for x in v.strip().splitlines()]))
            print("")


if __name__ == "__main__":
    go_through_dict(TEXTS)
