TEXTS = {
    "general": {
        "role": "Software Engineer",
        "desc": """
            I am a Software Engineer and Data Scientist, from Prague, currently based in --bold--London--.
            I graduated with distinction from the --bold--Czech Technical University in Prague-- and currently I'm studying a master's at 
            --bold--University College London--, in --bold--Web Science and Big Data Analytics--, a data science degree with a focus on machine learning.
            My programming language of choice is --bold--Python--. I've got a lot of experience with --bold--Django-- and --bold--Flask--.
            I prefer the backend side of things, but I've also developed a couple of projects using --bold--AngularJS-- for the frontend.
            I also have experience with DevOps (mainly on AWS) and I am a big fan of CI & CD.
        """,
        "web_desc": "Online CV of Mikuláš Poul",
    },
    "personal": {
        "modbus2tcp": {
            "name": "modbus2tcp-reader",
            "from": "2018",
            "to": "2021",
            "url": "https://gitlab.com/mikulaspoul/modbus2tcp-reader",
            "link_text": "mikulaspoul/modbus2tcp-reader",
            "desc": """
                A small project for reading and storing data from electricity meters.
                Originally for my father's school, ended up being deployed 
                  to analyse consumption of several buildings.
            """,
        },
        "bettersubbox": {
            "name": "BetterSubBox",
            "from": "2014",
            "to": "2019",
            "url": "https://gitlab.com/mikulaspoul/BetterSubBox",
            "link_text": "mikulaspoul/BetterSubBox",
            "desc": """
                A discontinued tool for YouTube power-users, extending the YouTube subscriptions with extra functionality, 
                  originally my graduation project in high school.
                Written in Python and CoffeeScript, backend in Django, frontend in Angular.
                Involved work with YouTube's API, asynchronous actions with celery, 
                  and optimising cache for performance.
            """,
            "short_desc": """
                A discontinued tool for YouTube power-users, extending the YouTube subscriptions, originally my high school graduation project.
                Backend in Django and AngularJS (with CoffeeScript).
            """,
        },
        "arca": {
            "name": "Arca",
            "from": "2018",
            "to": "2019",
            "url": "https://github.com/pyvec/arca",
            "link_text": "pyvec/arca",
            "desc": """
                Additional development of the tool Arca which was the principal part of my bachelor's thesis.
            """,
        },
        "p4a_portal": {
            "name": "P4A Portal",
            "from": "2022",
            "to": "2023",
            "url": "https://github.com/mikicz/p4a-portal",
            "link_text": "mikicz/p4a-portal",
            "desc": """
                Calculating statistics for the Project For Awesome charity livestream.
            """,
        },
        "pytest_unused": {
            "name": "pytest-unused-fixtures",
            "from": "2023",
            "to": "2023",
            "url": "https://github.com/mikicz/pytest-unused-fixtures",
            "link_text": "mikicz/pytest-unused-fixtures",
            "desc": """
                Pytest plugin for identifying unused fixtures. 
            """,
        },
        "pytest_xdist": {
            "name": "pytest-xdist-worker-stats",
            "from": "2023",
            "to": "2023",
            "url": "https://github.com/mikicz/pytest-xdist-worker-stats",
            "link_text": "mikicz/pytest-xdist-worker-stats",
            "desc": """
                Pytest plugin for collecting statistics about xdist workers. 
            """,
        },
    },
    "work": {
        "xelix": {
            "company": "Xelix",
            "url": "https://xelix.com/",
            "location": "London & Remote",
            "developer": {
                "role": "Software developer",
                "from": "October 2019",
                "to": "September 2022",
                "desc": """
                    Developing the backend of the Xelix platform using Django.
                    Wrote features, refactored or rewrote large parts of the codebase, scaling from a few
                    customers to many more with much more data.
                    Introduced linting, and better dependency management early on.
                    Focused on testing, writing custom tools, owning them running fast & reliably.
                    Did with DevOps as well, using Terraform in AWS.
                    Implemented data science team's algorithms and ML models.
                """,
            },
            "lead": {
                "role": "Lead Data Engineer",
                "from": "October 2022",
                "to": "July 2023",
                "desc": """
                    In addition to all the responsibilities of my previous role lead a small team with 
                    responsibility over the pipeline of data coming to Xelix, importing data from customer 
                    systems and running  processing and analysis on it. 
                    This included writing & orchestrating the pipeline, implementing algorithms and ML models, 
                    and performing the technical part of customer setup.
                    Line managed two team members directly, and mentoring other team members.
                    Communicated often with the product, customer success and implementation teams.
                    As a senior member of the tech team I was also involved in hiring and strategic planning.
                    Later in this role started focusing heavily on developer experience, making improvements to the
                    codebase, processes, infrastructure, and testing.
                """,
            },
            "staff": {
                "role": "Staff Engineer",
                "from": "August 2023",
                "to": "Ongoing",
                "desc": """"""
            }
        },
        "sparktech": {
            "role": "Full-stack developer",
            "company": "Eluvia (formerly SparkTECH)",
            "url": "https://www.eluvia.com/en/home",
            "from": "May 2014",
            "to": "September 2019",
            "location": "Prague & Remote",
            "desc": """
                Part-time. 
                Developing commissioned and in-house systems in Django and AngularJS.
                Gained experience with building new systems and maintaining older ones, reviewing code and 
                  being the owner of projects.
                In later years involved with DevOps, pioneering CI, handling automatic deployments and loadbalancing
                  using GitLab CI, AWS and custom-made tools.
            """,
            "short_desc": """
                Part-time. 
                Developing websites in Django and AngularJS.
                Gained experience with building new systems and maintaining old ones, reviewing code, being a project owner.
                Later got involved with DevOps, pioneering CI, handling continuous deployment and loadbalancing using GitLab CI and AWS.
            """,
        },
        "redhat": {
            "role": "Associate software engineer ‒ junior",
            "company": "Red Hat Czech",
            "url": "https://www.redhat.com/en",
            "from": "October 2017",
            "to": "June 2018",
            "location": "Remote",
            "desc": """
                Part-time.
                Supported by Red Hat to work on my bachelor's thesis to help the Czech Python community.
                The website --link--https://naucse.python.cz--naucse.python.cz--, enhanced by my thesis 
                  focuses on materials for teaching or learning Python.
            """,
        },
        "zoom": {
            "role": "Testcase writer",
            "company": "Eleveo (formally ZOOM International)",
            "url": "https://www.eleveo.com/",
            "from": "April 2012",
            "to": "May 2013",
            "location": "Prague",
            "desc": """
                Part-time. 
                Writing manuals on how to test complicated call centre --tex-newline--  applications, 
                  and testing them before new releases. 
            """,
        },
    },
    "school": {
        "ucl": {
            "name": "University College London",
            "degree": "MSc Web Science and Big Data Analytics",
            "from": "2018",
            "to": "2019",
            "location": "London",
            "desc": """
                A data science degree with a focus on the web and related concepts and technologies.
                Core modules focused on the structure of the web and --tex-newline-- retrieving information from it,
                  elective modules focused on data analytics and machine learning, including neural networks.
                  Graduated with distinction.
            """,
            "short_desc": """
                A data science degree with a focus on the web, data retrieval and machine learning including neural networks.
                Graduated with distinction.
            """,
            "thesis": {
                "title": "Fast calculation of the average \\\\ path length in large complex graphs",
                "subtitle": "Masters's thesis, available "
                "--link--https://masters-thesis.mikulaspoul.cz--online--",
                "desc": """
                   The thesis focused on speeding up the calculation of the exact average path length in large graphs,
                   using pruning for purposes of calculating the pair-wise distances.
                   In the second part that algorithm was used to further speed up an approximation using sampling.
                   The algorithm was implemented in C++ with Python bindings and extensively tested.
                """,
                "short_desc": """
                    My --link-highlight--https://masters-thesis.mikulaspoul.cz--thesis-- exlored speeding up the calculation
                    of the average path length in large graphs, 
                    both the the exact value and approximations.
                    Written in C++ and Python.  
                """,
            },
        },
        "cvut": {
            "name": "Faculty of Information Technology, \\\\ Czech Technical University in Prague",
            "name_without_newline": "Faculty of Information Technology, Czech Technical University in Prague",
            "degree": "Bc Informatics",
            "from": "2015",
            "to": "2018",
            "location": "Prague",
            "desc": """
                A general introduction to computer science, with a later specialisation in Web and Software Engineering,
                  which included technologies used on the web, Big Data databases, searching in web and multimedia, 
                  information retrieval and other web techniques.  
                Graduated with distinction and was awarded the Dean's Award for an excellent thesis.
            """,
            "short_desc": """
                Computer science degree with later specialisation in Web and Software Engineering.
                Graduated with distinction.
            """,
            "thesis": {
                "title": "Efficient and secure document rendering\\\\from multiple similar untrusted sources",
                "subtitle": "Bachelor's thesis, available "
                "--link--https://bachelors-thesis.mikulaspoul.cz--online--",
                "desc": """
                    A tool for running Python scripts from git repositories securely in separate environments 
                      with caching, OSS under MIT license.
                    Primarily used at --link--https://naucse.python.cz--naucse.python.cz-- to allow 
                      rendering of courses from forked repositories.
                    The thesis was defended with grade A and I was awarded the Dean's Award for 
                      an excellent thesis for it.
                """,
                "short_desc": """
                    My --link-highlight--https://bachelors-thesis.mikulaspoul.cz--thesis-- focused on running --tex-newline--
                    unknown Python code securely, using Docker.
                    Integrated the tool into a project for building content using Git repositories.
                    My thesis was awarded the Dean's Award for an excellent thesis.
                """,
            },
        },
        "arcig": {
            "name": "Archbishop Grammar School in Prague",
            "from": "2007",
            "to": "2015",
            "location": "Prague",
            "desc": """
                General education, but with a great computer science education as well, in electable subjects and 
                after-school activities, mostly in Python, basics of graph and graphics algorithms, 
                intro to logic programming, language design and interpreters and C++.
            """,
        },
    },
}
