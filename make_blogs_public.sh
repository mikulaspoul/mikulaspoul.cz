#!/bin/bash

for x in `ls blogs -1 | grep -v meta`; do
    echo $x;
    folder=`echo $x | sed -e "s/_/-/g" | sed -e "s/\.md//"`;
    echo $folder;
    mkdir public/$folder/;
    file=`echo $x | sed -e "s/\.md$/\.html/"`;
    cp "$file" public/$folder/index.html;
done