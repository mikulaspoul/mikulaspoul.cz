import json
from pathlib import Path

from embed_into_base import embed_into_base
from format_blog import format_blog

if __name__ == "__main__":
    blogs = list(Path("blogs").iterdir())
    for blog in blogs:
        if blog.name == "meta":
            continue

        stem = blog.stem
        blog_meta = json.loads(
            (Path("blogs") / "meta" / Path(stem + ".json")).read_text()
        )
        format_blog(blog, Path(stem + ".html"))
        embed_into_base(
            Path(stem + ".html"),
            title=blog_meta["title"] + " &middot; Mikuláš Poul",
            description=blog_meta["description"],
        )
