import argparse
import re
from pathlib import Path
import mistune
from mistune.plugins.formatting import _parse_to_end, PREVENT_BACKSLASH

_ANCHOR_END = re.compile(r"(?:" + PREVENT_BACKSLASH + r"\\~|[^\s~])~~(?!~)")


def existing_path(parser, arg):
    path = Path(arg)
    if not path.exists():
        parser.error("The file %s does not exist!" % arg)
    else:
        return path


def parse_anchor(inline, m, state):
    return _parse_to_end(inline, m, state, "anchor", _ANCHOR_END)


def render_anchor(renderer, text):
    return (
        f"<a name='{text}'></a><a href='#{text}'>"
        f"<span style='font-size: 0.4em; vertical-align: middle;'>&#128279;</span></a>"
    )


def anchor(md):
    md.inline.register(
        "anchor",
        r"~~(?=[^\s~])",
        parse_anchor,
        before="link",
    )
    if md.renderer and md.renderer.NAME == "html":
        md.renderer.register("anchor", render_anchor)


def format_blog(input_path: Path, output_path: Path):
    markdown = mistune.create_markdown(plugins=["footnotes", anchor], escape=False)
    text_html = markdown(input_path.read_text())

    output_path.write_text(
        Path("templates/blog.html").read_text().replace("--blog--", text_html)
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Formatting texts for various CV versions"
    )
    parser.add_argument(
        dest="input",
        help="Template file",
        metavar="INPUT",
        type=lambda x: existing_path(parser, x),
    )
    parser.add_argument(
        dest="output", help="Output file", metavar="OUTPUT", type=lambda x: Path(x)
    )

    args = parser.parse_args()

    format_blog(args.input, args.output)
