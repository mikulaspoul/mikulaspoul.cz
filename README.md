# Mikuláš Poul's Online CV

The source for the web and pdf versions of my CV.
Both versions are created using the same texts.
Refer to the Makefiles to see the process.

See LICENSE for license information.
