import argparse
import datetime
import json
import re
from pathlib import Path


from texts import TEXTS


def suffix(d):
    return "th" if 11 <= d <= 13 else {1: "st", 2: "nd", 3: "rd"}.get(d % 10, "th")


t = datetime.datetime.now()
TEXTS["time"] = t.strftime("%-H:%M %B {D}, %Y").replace(
    "{D}", f"{t.day}{suffix(t.day)}"
)
TEXTS["year"] = str(t.year)

TEXTS["blogs"] = {}
for file in Path("blogs/meta/").iterdir():
    if file.is_file():
        TEXTS["blogs"][file.stem] = json.loads(file.read_text())


placeholder_regex = re.compile(r"--[a-z_.0-9]+--")
link_regex = re.compile(r"--link--([a-z./:\-]+)--([a-zA-Z0-9.]+)--")
link_bold_regex = re.compile(r"--link-highlight--([a-z./:\-]+)--([a-zA-Z0-9.]+)--")
bold_regex = re.compile(r"--bold--([a-zA-Z0-9. ]+)--")


def existing_path(parser, arg):
    path = Path(arg)
    if not path.exists():
        parser.error("The file %s does not exist!" % arg)
    else:
        return path


def update_text_format(text, is_tex, is_html):
    if is_tex:
        text = text.replace("&", "\\&")
        text = text.replace("--tex-newline--", "\\\\")
    if is_html:
        text = text.replace("\\\\", "<br>")
        text = text.replace("--tex-newline--", "")

    for match in re.finditer(link_regex, text):
        original = match.group()
        groups = match.groups()

        if is_tex:
            new = "\\href{%s}{%s}" % (groups[0], groups[1])
        else:
            new = f'<a target="_blank" href="{groups[0]}">{groups[1]}</a>'

        text = text.replace(original, new)

    for match in re.finditer(link_bold_regex, text):
        original = match.group()
        groups = match.groups()

        if is_tex:
            new = "\\href{%s}{\\textit{\\underline{%s}}}" % (groups[0], groups[1])
        else:
            new = f'<a target="_blank" href="{groups[0]}">{groups[1]}</a>'

        text = text.replace(original, new)

    matches = re.finditer(bold_regex, text)

    for match in matches:
        original = match.group()
        groups = match.groups()

        if is_tex:
            new = "\\textbf{%s}" % groups[0]
        else:
            new = f"<strong>{groups[0]}</strong>"

        text = text.replace(original, new)

    return text


def format_file(input_path: Path, output_path: Path):
    text = input_path.read_text()
    filename = input_path.name

    is_tex = filename.endswith(".tex")
    is_html = filename.endswith(".html")

    for match in re.findall(placeholder_regex, text):
        string = match.replace("--", "").split(".")
        replacement = TEXTS
        for x in string:
            try:
                replacement = replacement[x]
            except KeyError as e:
                raise KeyError(f"{e}: out of {string}")

        replacement = update_text_format(replacement, is_tex, is_html)

        text = text.replace(match, replacement.strip())
        print(f"  Replacing {'.'.join(string)} in {filename}")

    output_path.write_text(text)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Formatting texts for various CV versions"
    )
    parser.add_argument(
        dest="input",
        help="Template file",
        metavar="INPUT",
        type=lambda x: existing_path(parser, x),
    )
    parser.add_argument(
        dest="output", help="Output file", metavar="OUTPUT", type=lambda x: Path(x)
    )

    args = parser.parse_args()

    format_file(args.input, args.output)
